/* OthelloApplet.java
 * The "Othello" game applet interface
 * @author Emmanuel Singletary (rkasper for the initial version)
 */
package othello;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class OthelloApplet extends JApplet implements ActionListener {

    public static int // constants to define size of board
            ROWS = 8,
            COLUMNS = 8,
            SQUARE_SIZE = 40;  // number of pixels for each square
    public static final char EMPTY = '.', // Represents an empty square.
            WHITE = 'W', // a white piece.
            BLACK = 'B', // a black piece.
            ILLEGAL = 'X';     // temporary mark for illegal move
    // the current player and opponent are either BLACK or WHITE
    // these variables are reset on each turn to allow the same methods
    // to be used for both players.
    private char player;
    private char opponent;

    public int consecutiveTurns = 0; //Variable to hold how many consecutive turns have passed
    public boolean computerOnly = false; // holds if it's computer v. computer
    public boolean twoPlayer = false; // holds if the game is local two player multiplayer

    // GUI components
    private Board gameBoard;        // stores the configuration of the game
    private JLabel topLabel;        // title shown at top of window
    private JLabel playerLabel;     // displays which player may move
    private JLabel gameStatus;      // available to display information about the game
    private JLabel piecesOfEachPlayer; // shows how many pieces each player has
    private JLabel whoIsWinning;    // shows who is currently winning
    private JLabel howManyPlaying;    // shows who is currently winning
    private JButton resetButton;    // allow player to end the current game, and start a new game
    private JButton simulateTurnButton; // allows a player to simulate the next turn using AI
    private Font boardFont;

    @Override
    public void init() {
        /* customize applet size and appearance */
        // total applet window size
        setSize((COLUMNS + 1) * SQUARE_SIZE + 150, ROWS * SQUARE_SIZE + 160);
        Container content = getContentPane();  // Content pane of applet.
        content.setLayout(null);  // place components at fixed positions
        content.setBackground(new Color(0, 0, 150));  // dark blue background.

        /* Create the components and add them to the applet. */
        boardFont = new Font("Arial", Font.BOLD, 14);
        topLabel = new JLabel("Othello (v1.0.0)", JLabel.CENTER); // Changed version string, we're rocking v1.0.0 now
        topLabel.setForeground(Color.WHITE);
        playerLabel = new JLabel("Black's Turn", JLabel.CENTER);
        playerLabel.setForeground(Color.YELLOW);
        playerLabel.setFont(boardFont);
        gameStatus = new JLabel("Begin game.", JLabel.CENTER);
        gameStatus.setForeground(Color.RED);
        gameStatus.setFont(boardFont);
        piecesOfEachPlayer = new JLabel("Black: 2  White: 2", JLabel.CENTER); // Shows how many pieces each player has
        piecesOfEachPlayer.setForeground(Color.WHITE);
        piecesOfEachPlayer.setFont(boardFont);
        whoIsWinning = new JLabel("New Game", JLabel.CENTER); // Shows who is currently winning (For those who can't figure it out based on pieces count :) )
        whoIsWinning.setForeground(Color.WHITE);
        whoIsWinning.setFont(boardFont);
        howManyPlaying = new JLabel("Unknown", JLabel.CENTER); // Shows what kind of game is going on (Computer v. Computer, Player 1 vs. Computer, etc)
        howManyPlaying.setForeground(Color.WHITE);
        howManyPlaying.setFont(boardFont);
        resetButton = new JButton("New Game");
        resetButton.addActionListener(this);
        simulateTurnButton = new JButton("Simulate Turn"); // A new button for the "simulate turn" feature
        simulateTurnButton.addActionListener(this);

        // set up the game board
        gameBoard = new Board();
        content.add(playerLabel);
        content.add(gameBoard);
        content.add(gameStatus);
        content.add(piecesOfEachPlayer);
        content.add(whoIsWinning);
        content.add(howManyPlaying);
        content.add(topLabel);
        content.add(resetButton);
        content.add(simulateTurnButton);

        // Set the position and size of interface components by calling
        // its setBounds() method.
        topLabel.setBounds(0, 10, 450, 30);
        howManyPlaying.setBounds(0, 30, 450, 30); //Puts this label below the version string
        playerLabel.setBounds(20 + COLUMNS * SQUARE_SIZE, 100, 140, 30);
        resetButton.setBounds(20 + COLUMNS * SQUARE_SIZE, 140, 140, 40);
        simulateTurnButton.setBounds(20 + COLUMNS * SQUARE_SIZE, 200, 140, 40); // Puts this button below the New Game button
        gameBoard.setBounds(10, 80, COLUMNS * SQUARE_SIZE, ROWS * SQUARE_SIZE);
        gameStatus.setBounds(10, 90 + ROWS * SQUARE_SIZE, COLUMNS * SQUARE_SIZE, 30);
        piecesOfEachPlayer.setBounds(10, 110 + ROWS * SQUARE_SIZE, COLUMNS * SQUARE_SIZE, 30); //Adds these strings below Game Status
        whoIsWinning.setBounds(10, 130 + ROWS * SQUARE_SIZE, COLUMNS * SQUARE_SIZE, 30);

        // Start the game
        newGame();
    }

    /* newGame method: reinitialize variables and interface components */
    public void newGame() {
        gameBoard.clearBoard();
        player = BLACK;
        opponent = WHITE;
        computerOnly = false; // Set these booleans to false everytime a new game is started
        twoPlayer = false;
        howManyPlaying.setText("Player 1 v. Computer"); // Default text, assuming the below conditionals don't pan out.
        int response = JOptionPane.showConfirmDialog(this, "Do you want the computer to play itself?", "Computer playing itself?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (response == JOptionPane.YES_OPTION) { // Asks if the player wants the computer to play itself. if so, set the boolean computerOnly to true, and change the howManyPlaying text.
            computerOnly = true;
            howManyPlaying.setText("Computer v. Computer");
        }
        if (!computerOnly) {
            int response2 = JOptionPane.showConfirmDialog(this, "Do you want a local two player game?", "Local two player?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (response2 == JOptionPane.YES_OPTION) { // Asks if the player wants to play with another player. if so, set the boolean computerOnly to true, and change the howManyPlaying text.
                twoPlayer = true;
                howManyPlaying.setText("Player 1 v. Player 2");
            }
        }
        playerLabel.setText((player == BLACK ? "Black" : "White") + "'s Turn");
        gameStatus.setText("Game Status");
        piecesOfEachPlayer.setText("Black: 2  White: 2"); // sets the initial piecesOfEachPlayer to the default of 2 for each
    }

    /* changePlayer method: called after a valid move by either player. */
    public void changePlayer() {
        if (!isGameOver()) {
            if (player == BLACK) { //Checks if the player is black, and reverses the colors.
                player = WHITE;
                opponent = BLACK;
            } else {
                player = BLACK;
                opponent = WHITE;
            }
            if (!gameBoard.anyLegalMove()) { // Checks if there are possible moves left
                if (!computerOnly) {
                    JOptionPane.showMessageDialog(this, (player == BLACK ? "Black" : "White") + " must pass. There is no possible move.");
                } // This dialogue box only shows if atleast one player is playing.
                consecutiveTurns++; //Increment consecutive turns
                //System.out.println(consecutiveTurns); Debug text
                changePlayer(); // Change the player (recursion please).
            }
            playerLabel.setText((player == BLACK ? "Black" : "White") + "'s Turn"); // Uses the same ternary operator as newGame.
            piecesOfEachPlayer.setText("Black: " + gameBoard.blackPieces + "  White: " + gameBoard.whitePieces); // Updae piecesOfEachPlayer string
            if (gameBoard.blackPieces == gameBoard.whitePieces) { // Also update who is currently winning.
                whoIsWinning.setText("Tied");
            } else if (gameBoard.blackPieces > gameBoard.whitePieces) {
                whoIsWinning.setText("Black is winning");
            } else {
                whoIsWinning.setText("White is winning");
            }
            consecutiveTurns = 0; // Set the consecutive turns to 0 if there IS a legal move.
            if (computerOnly) { // If it's comp v. comp, do a computer turn for both
                gameBoard.doComputerTurn();
            } else if (!computerOnly && !twoPlayer && player == WHITE) { // However, if it's a player v. computer game, and the current player is WHITE, then let the computer take its turn.
                gameBoard.doComputerTurn();
            } else {
                //Nothing at all, just let the second player go.
            }
        }
    }

    /* isGameOver method: returns whether game is over */
    public boolean isGameOver() {
        gameBoard.countPieces(); //Start by counting the pieces on the board.

        if (gameBoard.blackPieces == 0) { // This is called when there aren't any black pieces, similar statements and conditionals follow:
            JOptionPane.showMessageDialog(this, "White wins! (Black has no pieces.)");
            whoIsWinning.setText("White wins!");
            return true;
        } else if (gameBoard.whitePieces == 0) {
            JOptionPane.showMessageDialog(this, "Black wins! (White has no pieces.)");
            whoIsWinning.setText("Black wins!");
            return true;
        }
        if (gameBoard.blackPieces + gameBoard.whitePieces == 64) {
            if (gameBoard.blackPieces == gameBoard.whitePieces) {
                JOptionPane.showMessageDialog(this, "Tie game! (Full board equal pieces.)");
                whoIsWinning.setText("Tie game!");
                return true;
            } else if (gameBoard.blackPieces > gameBoard.whitePieces) {
                JOptionPane.showMessageDialog(this, "Black wins! (Full board more black pieces.)");
                whoIsWinning.setText("Black wins!");
                return true;
            } else {
                JOptionPane.showMessageDialog(this, "White wins! (Full board more white pieces.)");
                whoIsWinning.setText("White wins!");
                return true;
            }
        }
        if (consecutiveTurns == 2) { //If there are two consecutive turns, then obviously neither players have a legal move left.
            if (gameBoard.blackPieces == gameBoard.whitePieces) {
                JOptionPane.showMessageDialog(this, "Tie Game! (No legal moves, same pieces.)");
                whoIsWinning.setText("Tie game!");
                return true;
            } else if (gameBoard.blackPieces > gameBoard.whitePieces) {
                JOptionPane.showMessageDialog(this, "Black wins! (No legal moves more black pieces.)");
                whoIsWinning.setText("Black wins!");
                return true;
            } else {
                JOptionPane.showMessageDialog(this, "White wins! (No legal moves more white pieces.)");
                whoIsWinning.setText("White wins!");
                return true;
            }
        }
        return false;
    }

    /* actionPerformed method is called whenever the user clicks a button. */
    @Override
    public void actionPerformed(ActionEvent e) {
        // Use a separate branch to perform the action for each button
        if (e.getActionCommand().equals("New Game")) {
            int response = JOptionPane.showConfirmDialog(this, "Do you want to end this game?");
            if (response == JOptionPane.YES_OPTION) {
                newGame();
                gameBoard.repaint();
            }
        }
        if (e.getActionCommand().equals("Simulate Turn")) { // Basically, simulate turn lets you make the next move with AI, versus making the move manually.
            int response = JOptionPane.showConfirmDialog(this, "Simulate next turn with AI?", "Simulate next turn", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (response == JOptionPane.OK_OPTION) { //Logic for this button goes here.
                gameBoard.doComputerTurn();
            }
        }
    }

    // inner class to represent game board
    public class Board extends JPanel implements MouseListener {

        // variables to represent status of the game
        char[][] board;         // each board location is one of
        // EMPTY, BLACK, WHITE, ILLEGAL
        private int rows;             // dimensions of board 
        private int columns;          // initialized by constructor method
        private int blackPieces = 2;  // number of black pieces on board
        private int whitePieces = 2;  // number of white pieces on board

        // variables to control visible interface
        private int squareSize;           // size of each square in pixels
        private int panelLength, panelWidth;
        private Font boardFont;

        public Board() {
            // Constructor: create a board using default rows and columns
            rows = ROWS;
            columns = COLUMNS;
            squareSize = SQUARE_SIZE;
            panelLength = rows * squareSize;
            panelWidth = columns * squareSize;
            // dimensions of the board array are 2 greater than the actual board
            // to simplify checking possible actions for squares on the border
            board = new char[rows + 2][columns + 2];
            clearBoard();
            setBackground(Color.lightGray);
            addMouseListener(this); // listen for mouse clicks within the board panel
        } // end constructor

        /* Methods to update and access the board */

        /* clearBoard method */
        public void clearBoard() {
            // This loops through the entire board, setting every space to '.'
            for (int col = 0; col < columns + 2; col++) {
                for (int row = 0; row < rows + 2; row++) {
                    board[row][col] = EMPTY;
                }
            }
            //Set the 4 middle spaces to the propor colors.
            board[4][4] = 'W';
            board[4][5] = 'B';
            board[5][4] = 'B';
            board[5][5] = 'W';
        }

        /* isLegalMove method: return true if the current player can move at
         * the square given by (row, col)
         */
        public boolean isLegalMove(int row, int col) {
            if (board[row][col] != EMPTY) {
                return false;
            }
            // check each neighboring square, including diagonals
            for (int dy = -1; dy <= 1; dy++) {      // dy is change in y direction
                for (int dx = -1; dx <= 1; dx++) {  // dx is change in x direction
                    if (dy != 0 || dx != 0) {       // do not consider current square
                        int rowChange = dy; //Records the changes made so far
                        int colChange = dx;
                        if (board[row + rowChange][col + colChange] == opponent) { //Checks if the space being looked at is of the opponent's color
                            while (board[row + rowChange][col + colChange] == opponent) { //If it is, it follows the line of spaces.
                                rowChange += dy;
                                colChange += dx;
                            }
                            if (board[row + rowChange][col + colChange] == player) { //If it detects a player color at the end of the line, return true.
                                return true;
                            }
                        }
                    }
                }
            }
            return false; //Else, return fale :)
        }

        /* doMove method: place the current player's color at (row, col),
         * and flip the opponent's pieces in one or more directions that
         * should be reversed.
         * pre-condition: the current player has a legal move at (row, col)
         */
        public void doMove(int row, int col) {
            if (isLegalMove(row, col)) { //Starts by checking if it's a legal move (again, for safe measure).
                board[row][col] = player; //Sets the current space to the player's color.
                for (int dy = -1; dy <= 1; dy++) {      // dy is change in y direction
                    for (int dx = -1; dx <= 1; dx++) {  // dx is change in x direction
                        if (dy != 0 || dx != 0) {       // do not consider current square
                            int rowChange = dy; //Reusing logic from isLegalMove.
                            int colChange = dx;
                            if (board[row + rowChange][col + colChange] == opponent) {
                                int originCol = colChange; //Saves the space with the opponent for later changing.
                                int originRow = rowChange;
                                while (board[row + rowChange][col + colChange] == opponent) { //Checks if the current line ends with a player piece, without changing.
                                    rowChange += dy;
                                    colChange += dx;
                                }
                                if (board[row + rowChange][col + colChange] == player) { //If it does, actually go down the line a second time, changing pieces.
                                    while (board[row + originRow][col + originCol] == opponent) {
                                        board[row + originRow][col + originCol] = player;
                                        originRow += dy;
                                        originCol += dx;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            changePlayer(); //Changes the current player.
        }

        /* anyLegalMove method: returns true if there is any legal move
         * for the current player
         */
        public boolean anyLegalMove() {
            for (int col = 1; col <= columns; col++) { //Loops through the entire array, checking each space with isLegalMove.
                for (int row = 1; row <= rows; row++) {
                    if (isLegalMove(row, col)) {
                        return true;
                    }
                }
            }
            return false;
        }

        /* countPieces method: counts pieces of each color on board,
         * updating the instance variables blackPieces and whitePieces.
         * This will be called by isGameOver to determine if one of the players
         * has won the game.
         */
        public void countPieces() {
            int numOfBlack = 0; // temporary counter variables for countPieces()
            int numOfWhite = 0;
            for (int col = 1; col <= columns; col++) { //Loops through the entire array, checking each space with isLegalMove.
                for (int row = 1; row <= rows; row++) {
                    if (board[row][col] == BLACK && board[row][col] != EMPTY/*This is just extra insurance*/) {
                        numOfBlack++; //Everytime it finds a black piece, increment the counter for black pieces.
                    } else if (board[row][col] == WHITE && board[row][col] != EMPTY) {
                        numOfWhite++;
                    }
                }
            }
            blackPieces = numOfBlack; //Set the instance variables at the top of the class to what we found.
            whitePieces = numOfWhite;
        }

        /* doComputerTurn: optional method to make a move automatically
         * for the current player (implements a simple AI for one player)
         */
        public void doComputerTurn() {
            int bestMove = calculateBestMove(); // Calculate the best move based on the AI implementation
            int numPasses = 0; //Number of legal moves passed
            for (int col = 1; col <= columns; col++) { //Loops through the entire array, checking each space with isLegalMove.
                for (int row = 1; row <= rows; row++) {
                    if (isLegalMove(row, col)) {
                        if (numPasses == bestMove) { // If the number of passes equals the magic bestMove number, then execute the move.
                            doMove(row, col);
                            repaint();
                            return; // return so we don't move more than once!
                        }
                        numPasses++;
                    }
                }
            }
        }

        /* calculateBestMove: this method calculates the best move for
         * for the computer by searching the board for legal moves,
         * then finding how many pieces are flipped per move, then
         * finally calculating which move, and returning the number
         * of the legal move.
         */
        public int calculateBestMove() {
            int numberOfPossibleMoves = 0; // store the number of possible moves
            int numberOfChangedSpaces = 0; // store how many spaces were changed/
            int[] changedSpaces = new int[30]; // there IS a potential for an out-of-bounds exception, but I assume there won't be more than 20 potential moves
            // A potential design flaw, can be fixed with an ArrayList or something similar that doesn't have a defined set of elements.

            for (int col = 1; col <= columns; col++) { //Loops through the entire array, checking each space with isLegalMove.
                for (int row = 1; row <= rows; row++) {
                    if (isLegalMove(row, col)) {
                        numberOfPossibleMoves++; // Increments the number of legal moves, assigns each move with an index, from 0 to (hopefully) 29.
                        for (int dy = -1; dy <= 1; dy++) {      // dy is change in y direction
                            for (int dx = -1; dx <= 1; dx++) {  // dx is change in x direction
                                if (dy != 0 || dx != 0) {       // do not consider current square
                                    int rowChange = dy; //Reusing logic from isLegalMove.
                                    int colChange = dx;

                                    if (board[row + rowChange][col + colChange] == opponent) {
                                        int originCol = colChange; //Saves the space with the opponent for later changing.
                                        int originRow = rowChange;
                                        while (board[row + rowChange][col + colChange] == opponent) { //Checks if the current line ends with a player piece, without changing.
                                            rowChange += dy;
                                            colChange += dx;
                                        }
                                        if (board[row + rowChange][col + colChange] == player) { //If it does, actually go down the line a second time, changing pieces.
                                            while (board[row + originRow][col + originCol] == opponent) {
                                                numberOfChangedSpaces++; // increments the number of changed spaces each time it travels, instead of changing the spaces.
                                                originRow += dy;
                                                originCol += dx;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // we subtract 1 so we start at the index 0.
                        changedSpaces[numberOfPossibleMoves - 1] = numberOfChangedSpaces; // Adds the result to the integer array at the index of the move.
                        numberOfChangedSpaces = 0; //resets the number of changed spaces back to zero.
                    }
                }
            }
            // This is where the magic happens
            int largestValue = changedSpaces[0];  // we start by setting the supposed largestValue to the first value in changedSpaces[]
            int index = 0; // the index of the highest number of spaces changed.
            for (int i = 1; i < changedSpaces.length; i++) { // loops through the number of indexes
                if (changedSpaces[i] >= largestValue) { // if the current number is higher than the current highest
                    largestValue = changedSpaces[i]; // set the new highest number
                    index = i; // record the index of that number
                }
            }
            return index; // return the magic number, and we're done!
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g); // handles fill with background color

            /* Draw the board */
            g.setFont(boardFont);
            for (int row = 1; row <= rows; row++) {
                for (int col = 1; col <= columns; col++) {
                    // First, draw a square around the cell
                    g.setColor(Color.BLACK);
                    g.drawRect((col - 1) * squareSize, (row - 1) * squareSize,
                            squareSize, squareSize);

                    // Draw colored pieces on a non-empty square
                    if (board[row][col] != EMPTY) {
                        drawPiece(g, board[row][col], row, col);
                    }
                }
            }
        }  // end paintComponent()

        /**
         * Draw a piece in the square at (row,col). The color is specified by
         * the piece parameter, which should be either BLACK or WHITE.
         */
        private void drawPiece(Graphics g, char piece, int row, int col) {
            if (piece == WHITE) {
                g.setColor(Color.WHITE);
            } else if (piece == BLACK) {
                g.setColor(Color.BLACK);
            } else if (piece == ILLEGAL) {
                g.setColor(Color.RED);
            } else {
                return;  // unknown piece color
            }
            g.fillOval(2 + squareSize * (col - 1), 2 + squareSize * (row - 1),
                    squareSize - 4, squareSize - 4);
        }

        /* doClickSquare method: performs an action when the mouse is clicked
         * within the square located by (row, col) in the board array.
         */
        public void doClickSquare(int row, int col) {
            // DIAGNOSTIC CONSOLE OUTPUT: show click locations
            System.out.println("doClick at row=" + row + ", col=" + col);
            if (isLegalMove(row, col)) { //Checks if the move is legal, and if it is, do the move!
                doMove(row, col);
            } else {
                JOptionPane.showMessageDialog(this, "Error: illegal move."); //Else, show an error message.
            }
            repaint(); // refreshes board display after changing the board
        }

        /* methods to implement MouseListener interface */
        @Override
        public void mouseClicked(MouseEvent evt) {
            // Respond to a user click on the board.
            // Find the row and column that the user clicked
            int col = evt.getX() / squareSize;
            int row = evt.getY() / squareSize;
            System.out.println("mouseClicked at (" + evt.getX() + "," + evt.getY() + ")");
            if (col < 0 || col >= columns || row < 0 || row >= rows) {
                JOptionPane.showMessageDialog(this, "Error: click out of bounds"
                        + "row=" + row + ", column=" + col);
            } else // add one to coordinates, because row 0 and column 0 are not used.
            {
                doClickSquare(row + 1, col + 1);
            }
        }

        // other methods required by MouseListener interface
        @Override
        public void mousePressed(MouseEvent evt) {
        }

        @Override
        public void mouseReleased(MouseEvent evt) {
        }

        @Override
        public void mouseEntered(MouseEvent evt) {
        }

        @Override
        public void mouseExited(MouseEvent evt) {
        }

        // methods used by layout manager
        @Override
        public Dimension getPreferredSize() {
            // Specify desired size for this component.
            return new Dimension(panelWidth, panelLength);
        }

        @Override
        public Dimension getMinimumSize() {
            return new Dimension(panelWidth, panelLength);
        }

        @Override
        public Dimension getMaximumSize() {
            return new Dimension(panelWidth, panelLength);
        }

    } // end Board inner class

} // end OthelloApplet class
